terraform {
  required_providers {
    freeipa = {
      source  = "camptocamp/freeipa"
      version = ">=0.7.0"
    }
    cloudflare = {
      source  = "cloudflare/cloudflare"
      version = ">=4.4.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = ">=2.20.0"
    }
  }
}

provider "cloudflare" {
  api_token = var.cloudflare_token
}

provider "freeipa" {
  host     = var.freeipa_host
  username = var.freeipa_username
  password = var.freeipa_password
  insecure = var.freeipa_insecure
}

data "cloudflare_zone" "domain" {
  count = var.record_externalname == null ? 0 : 1
  name  = var.domain_externalname
}

resource "cloudflare_record" "external" {
  count   = var.record_externalname == null ? 0 : 1
  zone_id = data.cloudflare_zone.domain[count.index].id
  name    = var.record_externalname
  value   = var.record_externaltarget
  type    = var.record_externaltype
  proxied = var.cloudflare_proxied
  ttl     = var.cloudflare_proxied == true ? 1 : var.record_externalttl
}

resource "freeipa_dns_record" "internal" {
  count           = var.record_internalname == null ? 0 : 1
  dnszoneidnsname = "${var.domain_internalname}."
  idnsname        = var.record_internalname
  records         = var.record_internaltarget
  type            = var.record_internaltype
  dnsttl          = var.record_internalttl
}
