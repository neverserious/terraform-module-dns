variable "freeipa_host" { sensitive = true }
variable "freeipa_username" { sensitive = true }
variable "freeipa_password" { sensitive = true }
variable "freeipa_insecure" { default = false }
variable "cloudflare_proxied" { default = true }
variable "cloudflare_token" {
  sensitive = true
  default = "0000000000000000000000000000000000000000"
}
variable "domain_internalname" {
  sensitive = true
  default = null
}
variable "domain_externalname" {
  sensitive = true
  default = null
}
variable "record_internalname" {
    default = null
}
variable "record_externalname" {
    default = null
}
variable "record_internaltarget" {
  sensitive = true
  default = null
}
variable "record_externaltarget" {
  sensitive = true
  default = null
}
variable "record_internalttl" { default = 3600 }
variable "record_externalttl" { default = 3600 }
variable "record_internaltype" { default = "CNAME" }
variable "record_externaltype" { default = "CNAME" }
